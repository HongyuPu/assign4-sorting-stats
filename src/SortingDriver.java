/**
 * Assignment 4 for CS 1410
 * This program evaluates the bubble and selection sorts versus each other.
 *
 * @author James Dean Mathias
 */
public class SortingDriver {
    public static void main(String[] args) {
        System.out.println("--- Timing Results ---\n");
        for(int i = 1000; i <= 10000; i = i + 1000) { //incrementing by 1000
            System.out.printf("Number of items     : %d\n", i);
            System.out.printf("Bubble sort time    : %d ms\n", bubbleSort(generateNumbers(i)).getTimeInMs()); //prints statements
            System.out.printf("Selection sort time : %d ms\n", selectionSort(generateNumbers(i)).getTimeInMs());
            System.out.println();
        }

    }

    public static int[] generateNumbers(int howMany) {
        int[] array = new int[howMany]; //creates an array
        for(int i = 0; i < array.length; i++) {
            array[i] = (int)(Math.random() * 100000); //generates an numbers from 0 - 100000
        }
        return array; //returns the array
    }

    public static SortingStats bubbleSort(int[] data) {
        new SortingStats();
        SortingStats myStats = new SortingStats(); //calls SortingStats class
        myStats.setTime(System.currentTimeMillis()); //sets beginning time
        for(int i = 0; i < data.length; i++) { //number of runs through array
            for(int j = 1; j < data.length; j++) { //compares each integer
                if(data[j -1] > data[j]) { //swaps numbers
                    int temp = data[j - 1];
                    data[j -1] = data[j];
                    data[j] = temp;
                    myStats.incrementSwapCount(); //counts each swap
                }
                myStats.incrementCompareCount(); //counts the each compare
            }
        }
        myStats.setTime(System.currentTimeMillis() - myStats.getTimeInMs()); //total time it took to sort
        return myStats;
    }

    public static SortingStats selectionSort(int[] data) {
        new SortingStats();
        SortingStats myStats = new SortingStats(); //creates an instance of SortingStats class
        myStats.setTime(System.currentTimeMillis()); //sets start time
        for (int i = 0; i < data.length - 1; i++) { //number of runs through code
            int currentMinIndex = i;
            for(int j = i + 1; j < data.length; j++) { //finds min number
                myStats.incrementCompareCount();
                if (data[j] < data[currentMinIndex]) {
                    currentMinIndex = j;
                    myStats.incrementSwapCount();
                }
            }
            int smallerNumber = data[currentMinIndex]; //swaps the smallest number and beginning
            data[currentMinIndex] = data[i];
            data[i] = smallerNumber;
        }
        myStats.setTime(System.currentTimeMillis() - myStats.getTimeInMs()); //total time it took to sort
        return myStats;
    }
}
